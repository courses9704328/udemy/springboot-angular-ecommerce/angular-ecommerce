import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../models/Product';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseUrl: string = "http://localhost:8080/api/products";

  constructor(private httpClient: HttpClient) { }

  getListOfProducts(): Observable<Product[]> {
    return this.httpClient.get<getResponse>(this.baseUrl).pipe(
      map(response => response._embedded.products)
    );
  }

}

interface getResponse {
  _embedded: {
    products: Product[];
  }
}
